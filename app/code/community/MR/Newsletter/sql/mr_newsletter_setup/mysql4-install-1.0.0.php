<?php
$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE newsletter_subscriber ADD firstname varchar (255) NULL DEFAULT NULL;
ALTER TABLE newsletter_subscriber ADD gender varchar (255) NULL DEFAULT NULL;
");
$installer->endSetup();
?>